// SPDX-FileCopyrightText: 2021 Andrew Hayzen <ahayzen@gmail.com>
//
// SPDX-License-Identifier: MPL-2.0
import assert from "assert";

import { startApp, stopApp } from "./helper";

describe("Application launch", function () {
    this.timeout(10000);

    beforeEach(async function () {
        this.app = await startApp();
    });

    afterEach(async function () {
        await stopApp(this.app);
    });

    it("shows an initial window", async function () {
        const count = await this.app.client.getWindowCount();
        assert.strictEqual(count, 1);
    });

    it("window name is correct", async function () {
        const title = await this.app.client.getTitle();
        assert.strictEqual(title, "Hello World!");
    });
});
