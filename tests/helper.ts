// SPDX-FileCopyrightText: 2021 Andrew Hayzen <ahayzen@gmail.com>
//
// SPDX-License-Identifier: MPL-2.0
import electronPath from "electron";
import path from "path";
import { Application } from "spectron";

export async function startApp() {
    return new Application({
        args: [path.join(__dirname, "../main.js")],
        path: electronPath + "", // allow typescript to consider string version of path
    }).start();
}

export async function stopApp(app: Application) {
    if (app && app.isRunning()) {
        return app.stop();
    }
}
