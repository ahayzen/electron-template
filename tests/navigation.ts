// SPDX-FileCopyrightText: 2021 Andrew Hayzen <ahayzen@gmail.com>
//
// SPDX-License-Identifier: MPL-2.0
import assert from "assert";

import { startApp, stopApp } from "./helper";

describe("Menu", function () {
    this.timeout(10000);

    beforeEach(async function () {
        this.app = await startApp();
    });

    afterEach(async function () {
        await stopApp(this.app);
    });

    it("menu initial selection", async function () {
        const menu = await this.app.client.$("#menu");
        const router = await this.app.client.$("#router");

        // Check that initial state of menu and router are correct
        assert.strictEqual(await menu.getAttribute("selectedAction"), "home");
        assert.strictEqual(await router.getAttribute("route"), "home");
    });

    it("menu change to page", async function () {
        const menu = await this.app.client.$("#menu");
        const router = await this.app.client.$("#router");

        // Click Wasm menu
        const wasmMenu = await menu.$("[action='wasm']");
        await wasmMenu.click();

        // Check that menu and router have changed
        assert.strictEqual(await menu.getAttribute("selectedAction"), "wasm");
        assert.strictEqual(await router.getAttribute("route"), "wasm");
    });
});
