<!--
SPDX-FileCopyrightText: 2021 Andrew Hayzen <ahayzen@gmail.com>

SPDX-License-Identifier: CC0-1.0
-->

# Goals

  * Cross platform application
  * Ability to run on website
  * Near native backend performance
  * Formatting and linting
  * Acceptance and unit tests

# Technology Used

  * electron as runtime
  * lit for WebComponent creation
  * scss for styling
  * typescript for scripting
  * wasm-bindgen with rust-lang for backend

# Build Tools

  * cleancss for css minify
  * electron-builder for packaging
  * imagemin for static images
  * rollup for bundling javascript
  * sass for building scss
  * terser for javascript and html minify

# Formatters

  * rustfmt for rust
  * prettier for all others

# Linters

  * clippy for rust
  * eslint for typescript
  * reuse for licenses
  * stylelint for scss

# Testing

  * spectron using mocha for acceptance tests
  * wasm-bindgen-test using chromedriver for rust unit tests
  * web-test-runner using playwright and chai for TypeScript unit tests

# Polyfills

  * construct-style-sheets-polyfill
    * This allows for adoptedStyleSheets in shadowRoot https://caniuse.com/mdn-api_shadowroot_adoptedstylesheets

# To Investigate

  * CLI wasm export
  * Add quit button to ask if you want to save on Electron
  * Storage is going to be a problem for wasm
    * WASI
      * https://wasi.dev/
      * https://github.com/wasmerio/wasmer-js/tree/master/packages/wasmfs
      * https://github.com/GoogleChromeLabs/wasi-fs-access
  * debugging?
  * PWA
    * https://developer.mozilla.org/en-US/docs/Web/Progressive_web_apps
  * Work offline as webapp
    * https://developer.mozilla.org/en-US/docs/Web/Progressive_web_apps/Offline_Service_workers
  * https://github.com/postcss/autoprefixer
  * can wasm-bindgen-cli can be pinned via a Cargo.toml ?
  * after bumping version due to npm outdated, npm update didn't install, needed to run npm install?
  * What are
    found 18 moderate severity vulnerabilities
      run `npm audit fix` to fix them, or `npm audit` for details
  * wasm-opt
  * https://www.electronjs.org/docs/api/system-preferences#systempreferencesgetaccentcolor-windows-macos
