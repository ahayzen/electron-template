// SPDX-FileCopyrightText: 2021 Andrew Hayzen <ahayzen@gmail.com>
//
// SPDX-License-Identifier: CC0-1.0

module.exports = {
    printWidth: 120,
    semi: true,
    tabWidth: 4,
};
