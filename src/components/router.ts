// SPDX-FileCopyrightText: 2021 Andrew Hayzen <ahayzen@gmail.com>
//
// SPDX-License-Identifier: MPL-2.0

import { AdoptedStyleSheetsHelper } from "./base";
import { html, LitElement, TemplateResult } from "lit";
import { customElement, property, queryAssignedNodes, state } from "lit/decorators.js";
import { cache } from "lit/directives/cache.js";
import { unsafeHTML } from "lit/directives/unsafe-html.js";

@customElement("custom-router")
export class RouterShadow extends LitElement {
    @queryAssignedNodes("", true, "")
    routes!: NodeListOf<HTMLElement>;
    @property() route: string | null = null;

    // This tracks the current target route
    //
    // - if a duplicate route is requested it is ignored
    // - if an existing route is slow to fetch, and a new route is requested, the old is ignored
    private targetRoute: string | null = null;

    attributeChangedCallback(name: string, oldVal: string | null, newVal: string | null): void {
        super.attributeChangedCallback(name, oldVal, newVal);

        if (name === "route") {
            // Find the old route and the new route
            let oldRoute: Route | null = null;
            let newRoute: Route | null = null;
            for (const route of this.routes) {
                if (route instanceof Route) {
                    if (this.route === route.name) {
                        newRoute = route;
                    }

                    if (route.active) {
                        oldRoute = route;
                    }
                }
            }

            // Check these routes are not the same and the new route is valid
            if (oldRoute !== newRoute && newRoute && newRoute.href) {
                // If the target route is already this route then ignore
                if (this.targetRoute === newRoute.name) {
                    return;
                }

                // Indicate the latest target route
                this.targetRoute = newRoute.name;

                // Fetch the content of the new route
                newRoute.fetchContent().then(() => {
                    // Content has loaded so now perform the flip
                    //
                    // Check that the target route is still set to this page
                    if (newRoute && this.targetRoute === newRoute.name) {
                        // Signal that we are about to change the route
                        // this allows others to unbind from the route
                        if (oldRoute?.name) {
                            this.dispatchEvent(
                                new CustomEvent("routeBeforeChanged", {
                                    detail: {
                                        route: oldRoute?.name,
                                    },
                                })
                            );
                        }

                        // Flip which route is active
                        // this then triggers the routeAfterChanged in the Route
                        oldRoute?.removeAttribute("active");
                        newRoute.setAttribute("active", "true");

                        // We have updated the route so clear the target
                        this.targetRoute = null;
                    }
                });
            }
        }
    }

    render(): TemplateResult {
        return html`<slot></slot>`;
    }
}

@customElement("custom-route")
export class Route extends LitElement {
    @property({ type: Boolean }) active = false;
    @property() href: string | null = null;
    @property() name: string | null = null;
    @property() stylesheet: string | null = null;
    @state() private content: TemplateResult | null = null;

    private adoptedStyleSheetsHelper: AdoptedStyleSheetsHelper;

    constructor() {
        super();

        this.adoptedStyleSheetsHelper = new AdoptedStyleSheetsHelper(this);
    }

    async fetchContent(): Promise<void> {
        if (this.content === null && this.href) {
            // Fetch the HTML and CSS
            const promiseArray: Promise<string | CSSStyleSheet>[] = this.stylesheet
                ? [
                      this.fetchUrl(this.href),
                      ...AdoptedStyleSheetsHelper.COMMON_STYLE_SHEETS.map(
                          async (sheet) => await AdoptedStyleSheetsHelper.get_or_fetch(sheet)
                      ),
                      AdoptedStyleSheetsHelper.get_or_fetch(this.stylesheet),
                  ]
                : [
                      this.fetchUrl(this.href),
                      ...AdoptedStyleSheetsHelper.COMMON_STYLE_SHEETS.map(
                          async (sheet) => await AdoptedStyleSheetsHelper.get_or_fetch(sheet)
                      ),
                  ];
            const [contentHtml, ...contentCSS] = await Promise.all(promiseArray);

            // Load the HTML and CSS content
            this.adoptedStyleSheetsHelper.adoptSheets(...(contentCSS as CSSStyleSheet[]));
            this.content = html`${unsafeHTML(contentHtml as string)}`;
        }
    }

    private async fetchUrl(url: string): Promise<string> {
        const response = await fetch(url);
        return await response.text();
    }

    render(): TemplateResult {
        return html` ${cache(this.active ? this.content : null)} `;
    }

    updated(): void {
        // Signal that the route change is complete
        // this allows others to bind to the route
        if (this.active && this.parentElement instanceof RouterShadow) {
            this.parentElement.dispatchEvent(
                new CustomEvent("routeAfterChanged", {
                    detail: {
                        route: this.name,
                        shadowRoot: this.shadowRoot,
                    },
                })
            );
        }
    }
}
