// SPDX-FileCopyrightText: 2021 Andrew Hayzen <ahayzen@gmail.com>
//
// SPDX-License-Identifier: MPL-2.0
import "@lit-labs/virtualizer";
import { Layout1d } from "@lit-labs/virtualizer";
import { css, CSSResultGroup, html, LitElement, TemplateResult } from "lit";
import { customElement, property, queryAssignedNodes } from "lit/decorators.js";
import { directive } from "lit/directive.js";

import { AdoptedStyleSheetsHelper } from "./base";
import { TemplateComponentDirective } from "./directives";

const templateComponent = directive(TemplateComponentDirective);

@customElement("custom-listview")
export class ListView extends LitElement {
    // The model as an array of dictionary's
    @property({ attribute: "data-model", type: Object }) model: Array<Record<string, unknown>> | null = null;
    // The template delegate that has been passed in
    @queryAssignedNodes("delegate", true, "template") delegateQuery!: NodeListOf<Element>;

    private adoptedStyleSheetsHelper: AdoptedStyleSheetsHelper;
    // We need to create bound renderItem as when calls it lit-virtualizer we don't have this
    private boundRenderItem: (item: Record<string, unknown>, index: number) => TemplateResult;
    // This is the delegate to use for the listview
    private delegate: HTMLTemplateElement | null = null;
    // This tracks the current first rendered index for the listview, so that we can offset
    private firstRenderedIndex = 0;

    constructor() {
        super();

        this.adoptedStyleSheetsHelper = new AdoptedStyleSheetsHelper(this);
        this.boundRenderItem = this.renderItem.bind(this);
    }

    static get styles(): CSSResultGroup {
        return css`
            lit-virtualizer {
                display: flex;
                flex-direction: column;
                height: inherit;
            }

            .row {
                display: flex;
                flex-direction: row;
                padding-top: var(--gap);
                width: 100%;
            }
        `;
    }

    async firstUpdated(changedProperties: Map<string | number | symbol, unknown>): Promise<void> {
        super.firstUpdated(changedProperties);

        this.adoptedStyleSheetsHelper.adoptSheets(...(await AdoptedStyleSheetsHelper.COMMON_ADOPT_SHEETS()));

        // Find the delegate is one is set to the slot
        if (this.shadowRoot) {
            for (const delegate of this.delegateQuery) {
                if (delegate instanceof HTMLTemplateElement) {
                    this.delegate = delegate;
                    break;
                }
            }
        }
    }

    render(): TemplateResult {
        // We need to set layout otherwise we get the following error
        // "Cannot set property 'totalItems' of undefined"
        // https://github.com/PolymerLabs/uni-virtualizer/issues/103
        return html`
            <slot name="delegate"></slot>
            <lit-virtualizer
                .items=${this.model}
                .layout=${Layout1d}
                .renderItem=${this.boundRenderItem}
                @visibilityChanged=${this.handleVisibilityChanged}
            ></lit-virtualizer>
        `;
    }

    private renderItem(item: Record<string, unknown>, index: number): TemplateResult {
        // Save the firstRenderedIndex at the moment that we are renderng this item
        // otherwise if we scroll while events are occuring, the index in the emitted
        // events becomes incorrect as firstRenderedIndex changes.
        const firstRenderedIndex = this.firstRenderedIndex;
        return html`
            <section
                class="row"
                @change=${(e: Event) => this.handleChangedItem(e, firstRenderedIndex + index)}
                @delete=${() => this.handleDeleteItem(firstRenderedIndex + index)}
            >
                ${this.delegate ? templateComponent(this.delegate, item) : html``}
            </section>
        `;
    }

    private handleChangedItem(e: Event, index: number): void {
        if (e instanceof CustomEvent) {
            this.dispatchEvent(
                new CustomEvent("change", {
                    detail: {
                        index: index,
                        key: e.detail.key,
                        value: e.detail.value,
                    },
                })
            );
        }
    }

    private handleDeleteItem(index: number): void {
        this.dispatchEvent(
            new CustomEvent("delete", {
                detail: {
                    index: index,
                },
            })
        );
    }

    private handleVisibilityChanged(e: Event): void {
        if (e instanceof CustomEvent) {
            // Track the first renderered index so we can offset requests to the model
            //
            // Note this might be -1 before loading, so ensure we have at least 0
            this.firstRenderedIndex = Math.max(e.detail.first, 0) || 0;
        }
    }
}
