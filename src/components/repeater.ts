// SPDX-FileCopyrightText: 2021 Andrew Hayzen <ahayzen@gmail.com>
//
// SPDX-License-Identifier: MPL-2.0

import { css, CSSResultGroup, html, LitElement, TemplateResult } from "lit";
import { directive } from "lit/directive.js";
import { customElement, property, queryAssignedNodes } from "lit/decorators.js";
import { repeat } from "lit/directives/repeat.js";

import { AdoptedStyleSheetsHelper } from "./base";
import { TemplateComponentDirective } from "./directives";

const templateComponent = directive(TemplateComponentDirective);

@customElement("custom-repeater")
export class Repeater extends LitElement {
    // The model as an array of dictionary's
    @property({ attribute: "data-model", type: Object, reflect: true }) model: Array<Record<string, unknown>> = [];
    // The template delegate that has been passed in
    @queryAssignedNodes("delegate", true, "template") delegateQuery!: NodeListOf<Element>;

    private adoptedStyleSheetsHelper: AdoptedStyleSheetsHelper;
    private delegate: HTMLTemplateElement | null = null;

    constructor() {
        super();

        this.adoptedStyleSheetsHelper = new AdoptedStyleSheetsHelper(this);
    }

    static get styles(): CSSResultGroup {
        return css`
            :host {
                display: flex;
                flex-direction: column;
            }

            .row {
                display: flex;
                flex-direction: row;
                padding-top: var(--gap);
                width: 100%;
            }
        `;
    }

    async firstUpdated(changedProperties: Map<string | number | symbol, unknown>): Promise<void> {
        super.firstUpdated(changedProperties);

        this.adoptedStyleSheetsHelper.adoptSheets(...(await AdoptedStyleSheetsHelper.COMMON_ADOPT_SHEETS()));

        // Find the delegate is one is set to the slot
        if (this.shadowRoot) {
            for (const delegate of this.delegateQuery) {
                if (delegate instanceof HTMLTemplateElement) {
                    this.delegate = delegate;
                    break;
                }
            }
        }
    }

    render(): TemplateResult {
        return html`
            <slot name="delegate"></slot>

            ${repeat(
                this.model,
                (item) => Reflect.get(item, "id"),
                (item: Record<string, unknown>, index: number) => this.renderItem(item, index)
            )}
        `;
    }

    private renderItem(item: Record<string, unknown>, index: number): TemplateResult {
        return html`
            <section
                class="row"
                @change=${(e: Event) => this.handleChangedItem(e, index)}
                @delete=${() => this.handleDeleteItem(index)}
            >
                ${this.delegate ? templateComponent(this.delegate, item) : html``}
            </section>
        `;
    }

    private handleChangedItem(e: Event, index: number) {
        if (e instanceof CustomEvent) {
            this.dispatchEvent(
                new CustomEvent("change", {
                    detail: {
                        index: index,
                        key: e.detail.key,
                        value: e.detail.value,
                    },
                })
            );
        }
    }

    private handleDeleteItem(index: number) {
        this.dispatchEvent(
            new CustomEvent("delete", {
                detail: {
                    index: index,
                },
            })
        );
    }
}
