// SPDX-FileCopyrightText: 2021 Andrew Hayzen <ahayzen@gmail.com>
//
// SPDX-License-Identifier: MPL-2.0

import { noChange } from "lit";
import { Directive, PartInfo, PartType } from "lit/directive.js";

export class TemplateComponentDirective<T> extends Directive {
    private _previousChunk?: T;
    private _previousTemplate?: HTMLTemplateElement;

    constructor(partInfo: PartInfo) {
        super(partInfo);

        if (partInfo.type !== PartType.CHILD) {
            throw new Error("templateContent can only be used in child bindings");
        }
    }

    render(template: HTMLTemplateElement, item: T): DocumentFragment | symbol {
        // If the item and the template haven't changed then state no change
        if (this._previousChunk === item && this._previousTemplate === template) {
            return noChange;
        }
        this._previousChunk = item;
        this._previousTemplate = template;

        // Create a deep copy of the template that we can use as a document fragment later
        const frag = document.importNode(template.content, true);

        // Loop through each of the children of the <template> block
        for (const child of frag.children) {
            // Check if the child is an HTMLElement
            if (child instanceof HTMLElement) {
                // Attempt to set the given item to the model property or data attribute
                if (!Reflect.set(child, "model", item)) {
                    console.warn("Reflection failed, using slow path");

                    // Use JSON as we could not find item via reflection
                    child.dataset.model = JSON.stringify(item);
                }
            }
        }

        return frag;
    }
}
