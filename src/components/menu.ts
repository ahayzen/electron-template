// SPDX-FileCopyrightText: 2021 Andrew Hayzen <ahayzen@gmail.com>
//
// SPDX-License-Identifier: MPL-2.0

import { css, CSSResultGroup, html, LitElement, TemplateResult } from "lit";
import { customElement, property } from "lit/decorators.js";
import { classMap } from "lit/directives/class-map.js";

@customElement("custom-menu")
export class Menu extends LitElement {
    @property() selectedAction: string | null = null;

    static get styles(): CSSResultGroup {
        return css`
            .menu {
                list-style-type: none;
                margin: 0;
                padding: 0;
            }
        `;
    }

    attributeChangedCallback(name: string, oldVal: string | null, newVal: string | null): void {
        super.attributeChangedCallback(name, oldVal, newVal);

        if (name === "selectedaction") {
            for (const child of this.children) {
                if (child.tagName === "CUSTOM-MENUITEM") {
                    const menuItem = child as MenuItem;
                    menuItem.selected = newVal !== null && newVal === menuItem.action;
                }
            }
        }
    }

    render(): TemplateResult {
        return html`
            <ul class="menu">
                <slot></slot>
            </ul>
        `;
    }
}

@customElement("custom-menuitem")
export class MenuItem extends LitElement {
    @property() action: string | undefined;
    @property() href: string | undefined;
    @property({ type: Boolean, reflect: true }) selected = false;

    static get styles(): CSSResultGroup {
        return css`
            .menuitem__button,
            .menuitem__button:link,
            .menuitem__button:visited {
                color: var(--color);
                display: block;
                font-weight: normal;
                padding: 0.75rem 1rem;
                text-decoration: none;
            }

            .menuitem__button:hover {
                background-color: var(--color-hover);
            }

            .menuitem__button--selected,
            .menuitem__button--selected:hover,
            .menuitem__button--selected:link,
            .menuitem__button--selected:visited {
                background-color: var(--color-selected);
                color: var(--color-selected-text);
            }
        `;
    }

    handleClick(e: MouseEvent): void {
        if (this.action && this.parentElement?.tagName === "CUSTOM-MENU") {
            e.preventDefault();

            this.parentElement?.dispatchEvent(
                new CustomEvent("triggered", {
                    detail: {
                        action: this.action,
                        href: this.href,
                    },
                })
            );
        }
    }

    render(): TemplateResult {
        const classes = {
            "menuitem__button--selected": this.selected,
        };
        const href = this.action ? "#/" + this.action + ".html" : this.href || "#";
        return html`
            <li class="menuitem">
                <a class="menuitem__button ${classMap(classes)}" @click="${this.handleClick}" href="${href}">
                    <slot></slot>
                </a>
            </li>
        `;
    }
}
