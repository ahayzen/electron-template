// SPDX-FileCopyrightText: 2021 Andrew Hayzen <ahayzen@gmail.com>
//
// SPDX-License-Identifier: MPL-2.0
import { expect } from "chai";

import { Menu, MenuItem } from "../components";

describe("Menu", function () {
    it("menu selection action", () => {
        // Create a menu
        const menu = document.createElement("custom-menu");
        expect(menu).instanceof(Menu);

        // Create menu items
        const menuItemA = document.createElement("custom-menuitem") as MenuItem;
        const menuItemB = document.createElement("custom-menuitem") as MenuItem;
        expect(menuItemA).instanceof(MenuItem);
        expect(menuItemB).instanceof(MenuItem);

        // Add menu items to menu
        menu.append(menuItemA, menuItemB);

        // Set menu item actions
        menuItemA.setAttribute("action", "a");
        menuItemB.setAttribute("action", "b");

        // Set menu selection action
        menu.setAttribute("selectedAction", "a");

        // Check that selected value is correct
        expect(menuItemA.selected).to.equal(true);
        expect(menuItemB.selected).to.equal(false);

        // Set menu selection action
        menu.setAttribute("selectedAction", "b");

        // Check that selected value is correct
        expect(menuItemA.selected).to.equal(false);
        expect(menuItemB.selected).to.equal(true);

        // Clear node
        menu.remove();
    });

    it("menu clicked", () => {
        // Create a menu
        const menu = document.createElement("custom-menu");
        expect(menu).instanceof(Menu);

        // Create menu items
        const menuItemA = document.createElement("custom-menuitem") as MenuItem;
        const menuItemB = document.createElement("custom-menuitem") as MenuItem;
        expect(menuItemA).instanceof(MenuItem);
        expect(menuItemB).instanceof(MenuItem);

        // Add menu items to menu
        menu.append(menuItemA, menuItemB);

        // Add signal handler
        menu.addEventListener("triggered", (event: Event) => {
            if (event instanceof CustomEvent) {
                menu.setAttribute("selectedAction", event.detail.action);
            }
        });

        // Simulate shadow root events
        //
        // As we perform click events on the menuitem they don't come from the shadow root side
        // FIXME: for now we simulate any click events by manually calling the handler
        menuItemA.onclick = menuItemA.handleClick;
        menuItemB.onclick = menuItemB.handleClick;

        // Set menu item actions
        menuItemA.setAttribute("action", "a");
        menuItemB.setAttribute("action", "b");

        // Set menu selection action
        menuItemA.click();

        // Check that selected value is correct
        expect(menuItemA.selected).to.equal(true);
        expect(menuItemB.selected).to.equal(false);

        // Set menu selection action
        menuItemB.click();

        // Check that selected value is correct
        expect(menuItemA.selected).to.equal(false);
        expect(menuItemB.selected).to.equal(true);

        // Clear node
        menu.remove();
    });
});
