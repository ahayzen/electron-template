// SPDX-FileCopyrightText: 2021 Andrew Hayzen <ahayzen@gmail.com>
//
// SPDX-License-Identifier: MPL-2.0

const rollupCommonjs = require("@rollup/plugin-commonjs");
const rollupNodeResolve = require("@rollup/plugin-node-resolve");
const rollupSourcemaps = require("rollup-plugin-sourcemaps");

export default [
    {
        input: "build/compile/components/tests/components_test.js",
        output: {
            file: "build/bundle/components/tests/components_test.js",
            format: "es",
            inlineDynamicImports: true,
            sourcemap: true,
        },
        plugins: [
            rollupNodeResolve.nodeResolve(),
            // This allows for chai to work without needing esm-bundle
            rollupCommonjs(),
            rollupSourcemaps(),
        ],
    },
];
