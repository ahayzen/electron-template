// SPDX-FileCopyrightText: 2021 Andrew Hayzen <ahayzen@gmail.com>
//
// SPDX-License-Identifier: MPL-2.0

import { css, CSSResultGroup, html, LitElement, TemplateResult } from "lit";
import { customElement, property } from "lit/decorators.js";
import { repeat } from "lit/directives/repeat.js";

import { AdoptedStyleSheetsHelper } from "../base";

@customElement("custom-delegateinput")
export class DelegateInput extends LitElement {
    @property({
        type: Array,
        converter: {
            fromAttribute: (value, _type) => {
                return value?.split(",");
            },
            toAttribute: (value: Array<string>, _type) => {
                return value.join(",");
            },
        },
    })
    columns: Array<string> = [];
    @property({ attribute: "data-model", type: Object }) model: Record<string, unknown> = {};

    private adoptedStyleSheetsHelper: AdoptedStyleSheetsHelper;

    constructor() {
        super();

        this.adoptedStyleSheetsHelper = new AdoptedStyleSheetsHelper(this);
    }

    static get styles(): CSSResultGroup {
        return css`
            :host {
                display: flex;
                flex-direction: row;
                gap: var(--gap);
            }
        `;
    }

    async firstUpdated(changedProperties: Map<string | number | symbol, unknown>): Promise<void> {
        super.firstUpdated(changedProperties);

        this.adoptedStyleSheetsHelper.adoptSheets(...(await AdoptedStyleSheetsHelper.COMMON_ADOPT_SHEETS()));
    }

    private handleDeleteRow(e: Event) {
        e.preventDefault();

        this.parentElement?.dispatchEvent(new CustomEvent("delete"));
    }

    private handleInputChanged(e: Event, index: number) {
        if (e.target instanceof HTMLInputElement) {
            this.parentElement?.dispatchEvent(
                new CustomEvent("change", {
                    detail: {
                        key: this.columns[index],
                        value: e.target.value,
                    },
                })
            );
        }
    }

    render(): TemplateResult {
        return html`
            ${repeat(
                this.columns,
                (item: string) => item,
                (item: string, index: number) => this.renderItem(item, index)
            )}

            <button class="--negative" @click="${this.handleDeleteRow}">Delete Row</button>
        `;
    }

    private renderItem(item: string, index: number): TemplateResult {
        return html`<input
            @change=${(e: Event) => this.handleInputChanged(e, index)}
            @keyup=${(e: Event) => this.handleInputChanged(e, index)}
            type="text"
            value="${this.model ? Reflect.get(this.model, item) : ""}"
        />`;
    }
}
