// SPDX-FileCopyrightText: 2021 Andrew Hayzen <ahayzen@gmail.com>
//
// SPDX-License-Identifier: MPL-2.0

import { LitElement } from "lit";

// Provide a cache for CSSStyleSheet's by key-value
class CacheStyleSheet {
    private data: Map<string, CSSStyleSheet>;
    private static _instance: CacheStyleSheet;

    private constructor() {
        this.data = new Map();
    }

    // Retrieve the current cache instance
    static get instance(): CacheStyleSheet {
        if (!this._instance) {
            this._instance = new CacheStyleSheet();
        }

        return this._instance;
    }

    get(key: string): CSSStyleSheet | undefined {
        return this.data.get(key);
    }

    set(key: string, value: CSSStyleSheet) {
        this.data.set(key, value);
    }
}

export class AdoptedStyleSheetsHelper {
    private element: LitElement;
    private originalSheets: CSSStyleSheet[] | null = null;

    // Common style sheet urls
    //
    // These are used for LitElements that have nested ShadowDOM, so that we can pass through our
    // theme. Eg when there is a Router we want each inner route to have the common style sheets
    // and when there is a ListView we want to pass through to the delegates.
    static COMMON_STYLE_SHEETS: string[] = [];

    constructor(element: LitElement) {
        this.element = element;
    }

    // Build the common style sheets as CSSStyleSheet
    static async COMMON_ADOPT_SHEETS(): Promise<CSSStyleSheet[]> {
        return Promise.all(
            AdoptedStyleSheetsHelper.COMMON_STYLE_SHEETS.map(
                async (sheet) => await AdoptedStyleSheetsHelper.get_or_fetch(sheet)
            )
        );
    }

    // Adopt the given style sheets to the LitElement
    //
    // If called twice this will replaced previously adopted style sheets
    adoptSheets(...adoptedSheets: CSSStyleSheet[]): void {
        if (this.element.shadowRoot) {
            // Store any existing adoptedStyleSheets before we update them
            // so that if our extra adoptedStyleSheets changed, we can keep the existing ones.
            //
            // Note that LitElement for the static styles() uses adoptedStyleSheets
            if (this.originalSheets === null) {
                this.originalSheets = this.element.shadowRoot.adoptedStyleSheets.slice();
            }

            // Assign the original sheets and the extra ones
            if (this.originalSheets.length > 0) {
                this.element.shadowRoot.adoptedStyleSheets = [...this.originalSheets, ...adoptedSheets];
            } else {
                this.element.shadowRoot.adoptedStyleSheets = adoptedSheets;
            }
        }
    }

    // Fetch the CSSStyleSheet for the given url from cache or web
    static async get_or_fetch(url: string): Promise<CSSStyleSheet> {
        let sheet = CacheStyleSheet.instance.get(url);

        if (!sheet) {
            const response = await fetch(url);
            const text = await response.text();
            sheet = new CSSStyleSheet();
            await sheet.replace(text);
            CacheStyleSheet.instance.set(url, sheet);
        }

        return sheet;
    }
}

// Listen for common stylesheets from the settings
window.addEventListener("ShadowDOMStyleSheets", (e: Event) => {
    if (e instanceof CustomEvent) {
        const styleSheets = e.detail.styleSheets;
        if (styleSheets) {
            // Set the common style sheets
            AdoptedStyleSheetsHelper.COMMON_STYLE_SHEETS = styleSheets;
        }
    }
});
