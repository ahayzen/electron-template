// SPDX-FileCopyrightText: 2021 Andrew Hayzen <ahayzen@gmail.com>
//
// SPDX-License-Identifier: MPL-2.0

const rollupNodeResolve = require("@rollup/plugin-node-resolve");
const rollupSourcemaps = require("rollup-plugin-sourcemaps");

export default [
    {
        input: "build/compile/components/components.js",
        output: {
            file: "build/bundle/components/components.js",
            format: "es",
            inlineDynamicImports: true,
            sourcemap: true,
        },
        plugins: [rollupNodeResolve.nodeResolve(), rollupSourcemaps()],
    },
];
