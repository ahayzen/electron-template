// SPDX-FileCopyrightText: 2021 Andrew Hayzen <ahayzen@gmail.com>
//
// SPDX-License-Identifier: MPL-2.0

use crate::events::PubSub;
use serde::{Deserialize, Serialize};
use wasm_bindgen::prelude::*;
use web_sys::{window, CustomEvent, CustomEventInit, Storage};

#[derive(Serialize, Deserialize)]
struct ThemeChangedEvent {
    theme: String,
}

#[wasm_bindgen]
#[derive(Default)]
pub struct ThemeModel {
    events: PubSub,
    storage: Option<Storage>,
    theme: String,
}

#[wasm_bindgen]
impl ThemeModel {
    #[wasm_bindgen(constructor, typescript_type = "IModel")]
    pub fn new() -> Self {
        // Connect to the local storage of the window
        let storage = window()
            .expect("No global window exists")
            .local_storage()
            .or::<Option<Storage>>(Ok(None))
            .unwrap();
        // Attempt to extra a user theme
        let theme = if let Some(storage) = &storage {
            match storage.get_item("theme") {
                Ok(Some(theme)) => Some(theme),
                _ => None,
            }
        } else {
            None
        };

        Self {
            events: PubSub::new(),
            storage,
            // Use the user theme or fallback to system
            theme: theme.unwrap_or_else(|| "system".to_owned()),
        }
    }

    #[wasm_bindgen(getter)]
    pub fn theme(&self) -> String {
        self.theme.clone()
    }

    #[wasm_bindgen(setter)]
    pub fn set_theme(&mut self, new_theme: String) {
        if self.theme != new_theme {
            self.theme = new_theme.to_owned();

            // Update local storage
            if let Some(storage) = &self.storage {
                let _ = storage.set_item("theme", &new_theme);
            }

            // Publish event
            let event = CustomEvent::new_with_event_init_dict(
                "themeChanged",
                CustomEventInit::new()
                    .detail(&JsValue::from_serde(&ThemeChangedEvent { theme: new_theme }).unwrap()),
            );
            if let Ok(event) = event {
                self.publish(event.into());
            }
        }
    }

    // Until we can use traits with wasm_bindgen, we manually implement the IModel interface
    #[wasm_bindgen]
    pub fn publish(&mut self, event: JsValue) {
        self.events.publish(event);
    }

    #[wasm_bindgen]
    pub fn subscribe(&mut self, name: String, callback: js_sys::Function) {
        self.events.subscribe(name, callback);
    }

    #[wasm_bindgen]
    pub fn unsubscribe(&mut self, name: String, callback: js_sys::Function) {
        self.events.unsubscribe(name, callback)
    }
}

#[cfg(test)]
pub mod tests {
    use super::*;
    use crate::tests::{JsSpy, TestStorage};

    use wasm_bindgen_test::*;

    #[wasm_bindgen_test]
    fn test_existing() {
        TestStorage::default().clear().set("theme", "custom");

        // Check that a second model reads the custom theme from storage
        let model_second = ThemeModel::new();
        assert_eq!(model_second.theme(), "custom".to_owned());
    }

    #[wasm_bindgen_test]
    fn test_init() {
        TestStorage::default().clear();

        let model = ThemeModel::new();

        // Initial theme should be system
        assert_eq!(model.theme(), "system".to_owned());
    }

    #[wasm_bindgen_test]
    fn test_set_theme() {
        TestStorage::default().clear();

        let mut model = ThemeModel::new();
        assert_eq!(model.theme(), "system".to_owned());

        // Listen for theme change events with spy counter
        let spy = JsSpy::new("spyCounter");
        model.subscribe("themeChanged".to_owned(), spy.incrementer());
        assert_eq!(spy.count(), Some(0.0));

        // It should be possible to change a theme
        model.set_theme("custom".to_owned());
        assert_eq!(model.theme(), "custom".to_owned());

        // Check our counter increased
        assert_eq!(spy.count(), Some(1.0));
    }
}
