// SPDX-FileCopyrightText: 2021 Andrew Hayzen <ahayzen@gmail.com>
//
// SPDX-License-Identifier: MPL-2.0

use crate::{events::PubSub, models::dataitem::DataItem};
use std::cmp::max;
use wasm_bindgen::prelude::*;

#[wasm_bindgen]
pub struct DataModel {
    events: PubSub,
    data: Vec<DataItem>,
    id: u64,
}

impl Default for DataModel {
    fn default() -> Self {
        Self {
            events: PubSub::new(),
            data: vec![],
            id: 1,
        }
    }
}

#[wasm_bindgen]
impl DataModel {
    #[wasm_bindgen(constructor)]
    pub fn new() -> Self {
        Self::default()
    }

    #[wasm_bindgen]
    pub fn add_row(&mut self) {
        self.data.push(DataItem::new(self.id));
        self.id += 1;
    }

    #[wasm_bindgen]
    pub fn clear_rows(&mut self) {
        self.data.clear();
        self.id = 1;
    }

    #[wasm_bindgen]
    pub fn delete_row(&mut self, index: usize) {
        self.data.remove(index);
    }

    #[wasm_bindgen]
    pub fn load_data(&mut self, data: &str) {
        self.data = serde_json::from_str(data).unwrap();

        // Set the next id to the current maximum + 1
        self.id = self
            .data
            .iter()
            .fold(0, |acc, item| max(item.get_id() + 1, acc));
    }

    #[wasm_bindgen]
    pub fn row(&self, index: usize) -> Option<DataItem> {
        self.data.get(index).cloned()
    }

    #[wasm_bindgen]
    pub fn save_data(&mut self) -> String {
        serde_json::to_string(&self.data).unwrap()
    }

    #[wasm_bindgen]
    pub fn size(&self) -> usize {
        self.data.len()
    }

    #[wasm_bindgen]
    pub fn update_row(&mut self, index: usize, key: &str, value: &str) {
        if let Some(item) = self.data.get_mut(index) {
            match key {
                "firstName" => item.set_first_name(value),
                "lastName" => item.set_last_name(value),
                _ => {}
            };
        }
    }

    // Until we can use traits with wasm_bindgen, we manually implement the IModel interface
    #[wasm_bindgen]
    pub fn publish(&mut self, event: JsValue) {
        self.events.publish(event);
    }

    #[wasm_bindgen]
    pub fn subscribe(&mut self, name: String, callback: js_sys::Function) {
        self.events.subscribe(name, callback);
    }

    #[wasm_bindgen]
    pub fn unsubscribe(&mut self, name: String, callback: js_sys::Function) {
        self.events.unsubscribe(name, callback)
    }
}

#[cfg(test)]
pub mod tests {
    use super::*;

    use wasm_bindgen_test::*;

    static TEST_DATA_IN: &str = r#"
        [
            {
                "id": 1,
                "first_name": "a1",
                "last_name": "a2"
            },
            {
                "id": 3,
                "first_name": "b1",
                "last_name": "b2"
            },
            {
                "id": 5,
                "first_name": "c1",
                "last_name": "c2"
            }
        ]
    "#;
    static TEST_DATA_OUT: &str = r#"[{"first_name":"a1","last_name":"a2","id":1},{"first_name":"b1","last_name":"b2","id":3}]"#;

    #[wasm_bindgen_test]
    fn test_new() {
        let model = DataModel::new();
        assert_eq!(model.size(), 0);
        assert_eq!(model.id, 1);
    }

    #[wasm_bindgen_test]
    fn test_add() {
        let mut model = DataModel::new();
        model.add_row();
        assert_eq!(model.size(), 1);
        assert_eq!(model.id, 2);
    }

    #[wasm_bindgen_test]
    fn test_clear_rows() {
        let mut model = DataModel::new();
        model.load_data(TEST_DATA_IN);

        model.clear_rows();
        assert_eq!(model.size(), 0);
        assert_eq!(model.id, 1);
    }

    #[wasm_bindgen_test]
    fn test_delete_row() {
        let mut model = DataModel::new();
        model.load_data(TEST_DATA_IN);

        model.delete_row(2);
        assert_eq!(model.size(), 2);
        assert_eq!(model.id, 6);
    }

    #[wasm_bindgen_test]
    fn test_load_data() {
        let mut model = DataModel::new();
        model.load_data(TEST_DATA_IN);
        assert_eq!(model.size(), 3);
        assert_eq!(model.id, 6);
    }

    #[wasm_bindgen_test]
    fn test_row() {
        let mut model = DataModel::new();
        model.load_data(TEST_DATA_IN);

        let row = model.row(1);
        assert!(row.is_some());
        assert_eq!(row.as_ref().unwrap().get_first_name(), "b1");
        assert_eq!(row.as_ref().unwrap().get_id(), 3);
        assert_eq!(row.as_ref().unwrap().get_last_name(), "b2");
    }

    #[wasm_bindgen_test]
    fn test_save_data() {
        let mut model = DataModel::new();
        model.add_row();
        model.add_row();
        model.add_row();

        model.delete_row(1);

        model.update_row(0, "firstName", "a1");
        model.update_row(0, "lastName", "a2");

        model.update_row(1, "firstName", "b1");
        model.update_row(1, "lastName", "b2");

        let data = model.save_data();
        assert_eq!(data, TEST_DATA_OUT);
    }

    // Note that size is already tested in add/clear/delete

    #[wasm_bindgen_test]
    fn test_update_row() {
        let mut model = DataModel::new();
        model.load_data(TEST_DATA_IN);

        model.update_row(1, "firstName", "z1");
        model.update_row(1, "lastName", "z2");

        let row = model.row(1);
        assert!(row.is_some());
        assert_eq!(row.as_ref().unwrap().get_first_name(), "z1");
        assert_eq!(row.as_ref().unwrap().get_id(), 3);
        assert_eq!(row.as_ref().unwrap().get_last_name(), "z2");

        // Check that changing id doesn't work
        model.update_row(1, "id", "aa");

        let row = model.row(1);
        assert_eq!(row.as_ref().unwrap().get_id(), 3);
    }
}
