// SPDX-FileCopyrightText: 2021 Andrew Hayzen <ahayzen@gmail.com>
//
// SPDX-License-Identifier: MPL-2.0

use wasm_bindgen::prelude::*;

// Until we can use traits with wasm_bindgen, we manually create an interface
#[wasm_bindgen(typescript_custom_section)]
const IMODEL: &'static str = r#"
export interface PubSub {
    publish(e: Event): void;
    subscribe(name: string, callback: (e: Event) => void): void;
    unsubscribe(name: string, callback: (e: Event) => void): void;
}

export interface IModel extends PubSub {

}

export interface IDataModel {
    row(index: number): any;
    size(): number;
}
"#;

pub mod dataitem;
pub mod datamodel;
pub mod thememodel;
