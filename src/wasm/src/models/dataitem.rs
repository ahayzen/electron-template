// SPDX-FileCopyrightText: 2021 Andrew Hayzen <ahayzen@gmail.com>
//
// SPDX-License-Identifier: MPL-2.0

use serde::{Deserialize, Serialize};
use wasm_bindgen::prelude::*;

#[wasm_bindgen]
#[derive(Clone, Serialize, Deserialize)]
pub struct DataItem {
    first_name: String,
    last_name: String,
    id: u64,
}

impl Default for DataItem {
    fn default() -> Self {
        Self {
            first_name: "".to_owned(),
            last_name: "".to_owned(),
            id: 0,
        }
    }
}

#[wasm_bindgen]
impl DataItem {
    #[wasm_bindgen(constructor)]
    pub fn new(id: u64) -> Self {
        Self {
            id,
            ..Self::default()
        }
    }

    #[wasm_bindgen(getter = firstName)]
    pub fn get_first_name(&self) -> String {
        self.first_name.to_owned()
    }

    #[wasm_bindgen(getter = lastName)]
    pub fn get_last_name(&self) -> String {
        self.last_name.to_owned()
    }

    #[wasm_bindgen(getter = id)]
    pub fn get_id(&self) -> u64 {
        self.id
    }
}

impl DataItem {
    pub fn set_first_name(&mut self, value: &str) {
        self.first_name = value.to_owned();
    }

    pub fn set_last_name(&mut self, value: &str) {
        self.last_name = value.to_owned();
    }
}
