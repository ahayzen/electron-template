// SPDX-FileCopyrightText: 2021 Andrew Hayzen <ahayzen@gmail.com>
//
// SPDX-License-Identifier: MPL-2.0

use std::collections::BTreeMap;
use wasm_bindgen::prelude::*;
use web_sys::Event;

#[wasm_bindgen]
extern "C" {
    #[wasm_bindgen(js_namespace = console)]
    fn warn(s: &str);
}

#[wasm_bindgen]
pub struct PubSub {
    subscriptions: BTreeMap<String, Vec<js_sys::Function>>,
}

impl Default for PubSub {
    fn default() -> Self {
        Self {
            subscriptions: BTreeMap::new(),
        }
    }
}

#[wasm_bindgen]
impl PubSub {
    #[wasm_bindgen(constructor)]
    pub fn new() -> Self {
        Self::default()
    }

    #[wasm_bindgen]
    pub fn publish(&self, e: JsValue) {
        let event = Event::from(e.clone());
        let this = JsValue::null();
        for val in self.subscriptions.get(&event.type_()).unwrap_or(&vec![]) {
            if let Err(err) = val.call1(&this, &e) {
                warn(&format!(
                    "PubSub::Publish: Error calling callback {:?} for {}:\n{:?}",
                    val,
                    event.type_(),
                    err
                ));
            }
        }
    }

    #[wasm_bindgen]
    pub fn subscribe(&mut self, name: String, callback: js_sys::Function) {
        (*self.subscriptions.entry(name).or_insert_with(Vec::new)).push(callback);
    }

    #[wasm_bindgen]
    pub fn unsubscribe(&mut self, name: String, callback: js_sys::Function) {
        (*self.subscriptions.entry(name).or_insert_with(Vec::new)).retain(|val| *val != callback);
    }
}
