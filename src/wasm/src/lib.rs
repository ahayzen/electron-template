// SPDX-FileCopyrightText: 2021 Andrew Hayzen <ahayzen@gmail.com>
//
// SPDX-License-Identifier: MPL-2.0

pub mod events;
pub mod models;

#[cfg(test)]
pub mod tests {
    use wasm_bindgen_test::*;

    wasm_bindgen_test_configure!(run_in_browser);

    /// A basic named spy object that is used for counting how many times a js_sys::Function
    /// parameter is called.
    pub struct JsSpy {
        name: &'static str,
    }

    impl JsSpy {
        /// Construct a spy with the given name
        pub fn new(name: &'static str) -> Self {
            let value = Self { name };
            value.clear();
            value
        }

        /// Resets the current count for this spy back to zero
        pub fn clear(&self) {
            let js_init = js_sys::Function::new_no_args(&format!("window.{} = 0;", self.name));
            assert!(js_init.call0(&wasm_bindgen::JsValue::null()).is_ok());
        }

        /// Retieves the current count for this spy
        pub fn count(&self) -> Option<f64> {
            let js_get = js_sys::Function::new_no_args(&format!("return window.{};", self.name));
            js_get
                .call0(&wasm_bindgen::JsValue::null())
                .unwrap_or_else(|_| wasm_bindgen::JsValue::null())
                .as_f64()
        }

        /// Returns a js_sys::Function which increments this spy count when called
        pub fn incrementer(&self) -> js_sys::Function {
            js_sys::Function::new_no_args(&format!("window.{} += 1;", self.name))
        }
    }

    /// A basic helper for managing storage
    pub struct TestStorage {
        storage: Option<web_sys::Storage>,
    }

    impl Default for TestStorage {
        /// Construct a storage helper
        fn default() -> Self {
            Self {
                storage: web_sys::window()
                    .expect("No global window exists")
                    .local_storage()
                    .or::<Option<web_sys::Storage>>(Ok(None))
                    .unwrap(),
            }
        }
    }

    impl TestStorage {
        /// Clear the storage
        pub fn clear(self) -> Self {
            if let Some(storage) = &self.storage {
                let _ = storage.clear();
            }

            self
        }

        /// Set the key value in the storage
        pub fn set(self, key: &str, value: &str) -> Self {
            if let Some(storage) = &self.storage {
                let _ = storage.set_item(key, value);
            }

            self
        }
    }
}
