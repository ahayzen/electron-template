// SPDX-FileCopyrightText: 2021 Andrew Hayzen <ahayzen@gmail.com>
//
// SPDX-License-Identifier: MPL-2.0

// This needs to include interfaces provided by preload's exposeInMainWorld
declare global {
    interface ProcessVersions {
        chrome: string;
        electron: string;
        node: string;
    }

    interface Electron {
        nativeTheme: {
            themeSource(theme: "system" | "dark" | "light"): void;
        };
        versions: ProcessVersions;
    }

    interface Window {
        electron: Electron | undefined;
    }
}

export {};
