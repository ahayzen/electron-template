// SPDX-FileCopyrightText: 2021 Andrew Hayzen <ahayzen@gmail.com>
//
// SPDX-License-Identifier: MPL-2.0

import { IDataModel } from "./wasm";

// Wraps a Map to provide a type specific getter, this is useful for returning the derived class
// for a given key, rather than the concrete or abstract class.
export class MappedInterface<V> extends Map<string, V> {
    getType<T extends V>(TCtor: new (...args: unknown[]) => T, key: string): T | null {
        const value = this.get(key);
        if (value instanceof TCtor) {
            return value;
        } else {
            return null;
        }
    }
}

// A proxy handler from a given IDataModel to an Array
//
// This allows for only passing the required rows from wasm into javascript.
// eg this is used in ListView's to create a fake Array that only passes the delegates across
//
// This proxies the following methods
// Array.length -> IDataModel.size()
// Array[n] -> IDataModel.row(n)
export function proxyHandlerDataModelToArray<T>(model: IDataModel): ProxyHandler<Array<T>> {
    return {
        get: function (obj: Array<T>, prop: PropertyKey, receiver: unknown): unknown {
            if (typeof prop === "string") {
                if (prop === "length") {
                    return model.size();
                }

                // Usually requesting of an index comes through as a string
                const num = parseInt(prop);
                if (!isNaN(num)) {
                    return model.row(num);
                }
            } else if (typeof prop === "number") {
                return model.row(prop);
            }

            // Fallback to calling the original target
            return Reflect.get(obj, prop, receiver);
        },
    };
}
