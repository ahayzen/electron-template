// SPDX-FileCopyrightText: 2021 Andrew Hayzen <ahayzen@gmail.com>
//
// SPDX-License-Identifier: MPL-2.0
import { expect } from "chai";

import { ThemeAdapter } from "../adapters";
import { MappedInterface } from "../helpers";
import initWasm, { IModel, ThemeModel } from "@external/wasm";

async function wasmCompile(): Promise<void> {
    await initWasm();
}

describe("ThemeAdapter", function () {
    beforeEach(async () => {
        // Compile wasm and fake electron
        await wasmCompile();
    });

    it("theme attach", async () => {
        // Create a ThemeAdapter
        const adapter = new ThemeAdapter();
        const models = new MappedInterface<IModel>([]);
        const themeModel = new ThemeModel();
        models.set("theme", themeModel);
        const view = window;
        const style = view.getComputedStyle(view.document.body);

        // Configure initial CSS variables
        const styleElm = document.createElement("style");
        document.head.appendChild(styleElm);
        styleElm.sheet?.insertRule(`
            :root {
                --theme-dark-text: #000000;
                --theme-light-text: #FFFFFF;
                --palette-text: var(--theme-light-text);
            }
        `);
        styleElm.sheet?.insertRule(`
            :root [data-user-theme="dark"] {
                --palette-text: var(--theme-dark-text);
            }
        `);
        styleElm.sheet?.insertRule(`
            :root [data-user-theme="light"] {
                --palette-text: var(--theme-light-text);
            }
        `);

        // Attach our adapter to the view and models
        await adapter.attach(view, models);
        expect(themeModel.theme).to.equal("system");
        expect(view.document.body.hasAttribute("data-user-theme")).to.equal(false);
        expect(style.getPropertyValue("--palette-text").trim()).to.equal("#FFFFFF");

        // Theme and CSS should change to dark
        themeModel.theme = "dark";
        expect(themeModel.theme).to.equal("dark");
        expect(view.document.body.getAttribute("data-user-theme")).to.equal("dark");
        expect(style.getPropertyValue("--palette-text").trim()).to.equal("#000000");

        // Theme and CSS should change to light
        themeModel.theme = "light";
        expect(themeModel.theme).to.equal("light");
        expect(view.document.body.getAttribute("data-user-theme")).to.equal("light");
        expect(style.getPropertyValue("--palette-text").trim()).to.equal("#FFFFFF");

        // Theme and CSS should change to light
        themeModel.theme = "system";
        expect(themeModel.theme).to.equal("system");
        expect(view.document.body.hasAttribute("data-user-theme")).to.equal(false);
        expect(style.getPropertyValue("--palette-text").trim()).to.equal("#FFFFFF");

        document.head.removeChild(styleElm);
    });
});
