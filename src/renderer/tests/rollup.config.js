// SPDX-FileCopyrightText: 2021 Andrew Hayzen <ahayzen@gmail.com>
//
// SPDX-License-Identifier: MPL-2.0

const rollupCommonjs = require("@rollup/plugin-commonjs");
const rollupNodeResolve = require("@rollup/plugin-node-resolve");
const rollupReplace = require("@rollup/plugin-replace");
const rollupSourcemaps = require("rollup-plugin-sourcemaps");

export default [
    {
        input: "build/compile/renderer/tests/renderer_test.js",
        output: {
            file: "build/bundle/renderer/tests/renderer_test.js",
            format: "es",
            sourcemap: true,
        },
        plugins: [
            rollupReplace({
                // TODO: This could filter to only imports
                delimiters: ["", ""],
                preventAssignment: true,
                values: {
                    "@external/": "../",
                },
            }),
            rollupNodeResolve.nodeResolve(),
            // This allows for chai to work without needing esm-bundle
            rollupCommonjs(),
            rollupSourcemaps(),
        ],
    },
];
