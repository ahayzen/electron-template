// SPDX-FileCopyrightText: 2021 Andrew Hayzen <ahayzen@gmail.com>
//
// SPDX-License-Identifier: MPL-2.0

// Include polyfill which allows Firefox to have adoptedStyleSheets
//
// https://developer.mozilla.org/en-US/docs/Mozilla/Firefox/Experimental_features#constructable_stylesheets
import "construct-style-sheets-polyfill";

import { AppWindow } from "./appwindow";

let app: AppWindow | null = null;

window.addEventListener("DOMContentLoaded", function (e: Event) {
    // Create our app
    app = new AppWindow();
    // Compile wasm
    app.wasmCompile()
        // Register models, adapters, presenters
        .then(async () => {
            await app?.registerTypes();
        })
        // Attach to a window
        .then(async () => {
            if (e.target instanceof Document && e.target.defaultView) {
                await app?.attach(e.target.defaultView);
            }
        })
        // Restore state or use default state
        .then(async () => {
            const defaultState = "home";
            app?.restoreState(defaultState);
        });
});
