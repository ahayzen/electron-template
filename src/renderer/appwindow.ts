// SPDX-FileCopyrightText: 2021 Andrew Hayzen <ahayzen@gmail.com>
//
// SPDX-License-Identifier: MPL-2.0

import { ElectronAdapter, IAdapter, PageAdapter, PageHistoryState, ThemeAdapter } from "./adapters";
import { MappedInterface } from "./helpers";
import { HomePresenter, IPresenter, WasmPresenter } from "./presenters";
import initWasm, { DataModel, IModel, ThemeModel } from "./wasm";

export class AppWindow {
    private adapters: MappedInterface<IAdapter> = new MappedInterface();
    private models: MappedInterface<IModel> = new MappedInterface();
    private presenters: MappedInterface<IPresenter> = new MappedInterface();

    private boundHandleRouterAfterChanged: (e: Event) => void;
    private boundHandleRouterBeforeChanged: (e: Event) => void;
    private routerElement: HTMLElement | null = null;

    constructor() {
        this.boundHandleRouterAfterChanged = this.handleRouterAfterChanged.bind(this);
        this.boundHandleRouterBeforeChanged = this.handleRouterBeforeChanged.bind(this);
    }

    // Register an adapter
    adapter(name: string, value: IAdapter): void {
        this.adapters.set(name, value);
    }

    async attach(view: Window): Promise<void> {
        // Listen to the route change, so that we can attach / detach the presenters
        this.routerElement = view.document.getElementById("router");
        this.routerElement?.addEventListener("routeAfterChanged", this.boundHandleRouterAfterChanged);
        this.routerElement?.addEventListener("routeBeforeChanged", this.boundHandleRouterBeforeChanged);

        // Attach all the adapters to the window
        const promiseArray: Array<Promise<void>> = [];
        for (const adapter of this.adapters.values()) {
            promiseArray.push(adapter.attach(view, this.models));
        }
        await Promise.all(promiseArray);
    }

    async detach(view: Window): Promise<void> {
        // Detach all the adapters to the window
        const promiseArray: Array<Promise<void>> = [];
        for (const adapter of this.adapters.values()) {
            promiseArray.push(adapter.detach(view, this.models));
        }
        await Promise.all(promiseArray);

        this.routerElement?.removeEventListener("routeAfterChanged", this.boundHandleRouterAfterChanged);
        this.routerElement?.removeEventListener("routeBeforeChanged", this.boundHandleRouterBeforeChanged);
        this.routerElement = null;
    }

    // Attach the presenter for the new page
    private handleRouterAfterChanged(e: Event) {
        if (e instanceof CustomEvent) {
            const route = e.detail.route;
            const shadowRoot = e.detail.shadowRoot;
            if (shadowRoot instanceof ShadowRoot) {
                this.presenters.get(route)?.attach(shadowRoot, this.models);
            }
        }
    }

    // Detach the presenter for the old page
    private handleRouterBeforeChanged(e: Event) {
        if (e instanceof CustomEvent) {
            const route = e.detail.route;
            const shadowRoot = e.detail.shadowRoot;
            if (shadowRoot instanceof ShadowRoot) {
                this.presenters.get(route)?.detach(shadowRoot, this.models);
            }
        }
    }

    // Register a model
    model(name: string, value: IModel): void {
        this.models.set(name, value);
    }

    // Register a presenter
    presenter(route: string, value: IPresenter): void {
        this.presenters.set(route, value);
    }

    async registerTypes(): Promise<void> {
        // Register models
        this.model("data", new DataModel());
        this.model("theme", new ThemeModel());

        // Register adapters
        this.adapter("page", new PageAdapter());
        this.adapter("theme", new ThemeAdapter());

        // If we are inside electron then register special adapter
        if (window.electron) {
            this.adapter("electron", new ElectronAdapter());
        }

        // Register presenters
        this.presenter("home", new HomePresenter());
        this.presenter("wasm", new WasmPresenter());
    }

    restoreState(defaultState: string): void {
        // Load the default page or read from URL
        const pageAdapter = this.adapters.getType(PageAdapter, "page");
        if (pageAdapter) {
            const hashPage = pageAdapter.hashToPage();
            pageAdapter.loadPage(hashPage || defaultState, PageHistoryState.ReplaceState);
        }
    }

    // Allow for wasm to be built, then return once we are ready
    async wasmCompile(): Promise<void> {
        await initWasm();
    }
}
