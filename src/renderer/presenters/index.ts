// SPDX-FileCopyrightText: 2021 Andrew Hayzen <ahayzen@gmail.com>
//
// SPDX-License-Identifier: MPL-2.0

import { MappedInterface } from "../helpers";
import { IModel } from "../wasm";

export interface IPresenter {
    attach(view: ShadowRoot, models: MappedInterface<IModel>): void;
    detach(view: ShadowRoot, models: MappedInterface<IModel>): void;
}

export * from "./homepresenter";
export * from "./wasmpresenter";
