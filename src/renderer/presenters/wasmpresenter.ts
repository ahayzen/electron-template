// SPDX-FileCopyrightText: 2021 Andrew Hayzen <ahayzen@gmail.com>
//
// SPDX-License-Identifier: MPL-2.0

import { MappedInterface, proxyHandlerDataModelToArray } from "../helpers";
import { IPresenter } from "./index";
import { DataItem, DataModel, IModel } from "../wasm";

export class WasmPresenter implements IPresenter {
    private boundHandleAddRow: (e: Event) => void;
    private boundHandleClearRows: (e: Event) => void;
    private boundHandleChangeRow: (e: Event) => void;
    private boundHandleDeleteRow: (e: Event) => void;
    private boundHandleFileInput: (e: Event) => void;
    private boundHandleFileSaveAs: (e: MouseEvent) => void;

    private dataModel: DataModel | null = null;
    private listView: HTMLElement | null = null;

    constructor() {
        this.boundHandleAddRow = this.handleAddRow.bind(this);
        this.boundHandleClearRows = this.handleClearRows.bind(this);
        this.boundHandleChangeRow = this.handleChangeRow.bind(this);
        this.boundHandleDeleteRow = this.handleDeleteRow.bind(this);
        this.boundHandleFileInput = this.handleFileInput.bind(this);
        this.boundHandleFileSaveAs = this.handleFileSaveAs.bind(this);
    }

    attach(view: ShadowRoot, models: MappedInterface<IModel>): void {
        this.dataModel = models.getType(DataModel, "data");

        view.getElementById("header__addRow")?.addEventListener("click", this.boundHandleAddRow);
        view.getElementById("header__new")?.addEventListener("click", this.boundHandleClearRows);
        view.getElementById("header__open")?.addEventListener("click", this.boundHandleFileInput);
        view.getElementById("header__save")?.addEventListener("click", this.boundHandleFileSaveAs);

        this.listView = view.getElementById("listView");
        this.listView?.addEventListener("change", this.boundHandleChangeRow);
        this.listView?.addEventListener("delete", this.boundHandleDeleteRow);
        this.updateRepeater();
    }

    detach(view: ShadowRoot, _models: MappedInterface<IModel>): void {
        this.listView?.addEventListener("change", this.boundHandleChangeRow);
        this.listView?.addEventListener("delete", this.boundHandleDeleteRow);
        this.listView = null;

        view.getElementById("header__addRow")?.removeEventListener("click", this.boundHandleAddRow);
        view.getElementById("header__new")?.removeEventListener("click", this.boundHandleClearRows);
        view.getElementById("header__open")?.removeEventListener("change", this.boundHandleFileInput);
        view.getElementById("header__save")?.removeEventListener("click", this.boundHandleFileSaveAs);

        this.dataModel = null;
    }

    private handleAddRow(): void {
        this.dataModel?.add_row();
        this.updateRepeater();
    }

    private handleChangeRow(e: Event): void {
        if (e instanceof CustomEvent) {
            this.dataModel?.update_row(e.detail.index, e.detail.key, e.detail.value);
        }
    }

    private handleClearRows(e: Event): void {
        e.preventDefault();

        this.dataModel?.clear_rows();
        this.updateRepeater();
    }

    private handleDeleteRow(e: Event): void {
        if (e instanceof CustomEvent) {
            this.dataModel?.delete_row(e.detail.index);
            this.updateRepeater();
        }
    }

    private handleFileInput(e: Event): void {
        e.preventDefault();

        // Show an Open File Dialog
        const input = document.createElement("input");
        input.multiple = false;
        input.type = "file";

        input.addEventListener("change", () => {
            // Read the files back
            for (const file of input.files || []) {
                file.slice(0, file.size)
                    .text()
                    .then((data) => {
                        // Load the data into the model
                        this.dataModel?.load_data(data);
                        this.updateRepeater();
                    });
            }
        });

        input.click();
    }

    private handleFileSaveAs(e: MouseEvent): void {
        // Collect data to save
        const data = this.dataModel?.save_data();
        if (data) {
            e.preventDefault();

            // Show a Save File dialog
            const a = document.createElement("a");
            const file = new Blob([data], { type: "text/plain" });
            a.href = URL.createObjectURL(file);
            a.download = "test.txt";
            a.click();

            // Release the object
            URL.revokeObjectURL(a.href);
        }
    }

    private updateRepeater() {
        if (this.listView && this.dataModel) {
            // TODO: can we cache the proxyModel ?
            // the problem seems to be that reactive property does not notice a change?
            // can we manually indicate that it has changed somehow?
            const proxyModel = new Proxy([], proxyHandlerDataModelToArray<DataItem>(this.dataModel));

            // Attempt to use reflection
            if (!Reflect.set(this.listView, "model", proxyModel)) {
                console.warn("Reflection failed, using slow path");

                // Build an array of the model
                const model = [];
                for (let i = 0, iMax = this.dataModel.size(); i < iMax; i++) {
                    model.push(this.dataModel.row(i));
                }

                // Use JSON as we could not find item via reflection
                this.listView.dataset.model = JSON.stringify(model);
            }
        }
    }
}
