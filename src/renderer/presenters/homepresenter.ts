// SPDX-FileCopyrightText: 2021 Andrew Hayzen <ahayzen@gmail.com>
//
// SPDX-License-Identifier: MPL-2.0

import { MappedInterface } from "../helpers";
import { IPresenter } from "./index";
import { IModel, ThemeModel } from "../wasm";

export class HomePresenter implements IPresenter {
    private boundHandleThemeSelector: (e: Event) => void;
    private boundOnThemeChanged: (e: Event) => void;
    private view: ShadowRoot | null = null;
    private themeModel: ThemeModel | null = null;

    constructor() {
        this.boundHandleThemeSelector = this.handleThemeSelectorTriggered.bind(this);
        this.boundOnThemeChanged = this.onThemeChanged.bind(this);
    }

    attach(view: ShadowRoot, models: MappedInterface<IModel>): void {
        this.view = view;

        this.themeModel = models.getType(ThemeModel, "theme");
        if (this.themeModel) {
            this.themeModel.subscribe("themeChanged", this.boundOnThemeChanged);

            for (const item of view.querySelectorAll(".theme-selector")) {
                item.addEventListener("triggered", this.boundHandleThemeSelector);
                item.setAttribute("selectedAction", this.themeModel.theme);
            }
        }

        // TODO: how should we pass this info ? It should be a IModel
        if (window.electron) {
            const versions = window.electron.versions;
            this.setVersion(view, "nodeVersion", versions.node);
            this.setVersion(view, "chromeVersion", versions.chrome);
            this.setVersion(view, "electronVersion", versions.electron);
        } else {
            const versionMessage = this.view.getElementById("versionMessage");
            if (versionMessage) {
                versionMessage.style.display = "none";
            }
        }
    }

    detach(view: ShadowRoot, _models: MappedInterface<IModel>): void {
        const versionMessage = view.getElementById("versionMessage");
        if (versionMessage) {
            versionMessage.style.display = "";
        }

        for (const item of view.querySelectorAll(".theme-selector")) {
            item.removeEventListener("triggered", this.boundHandleThemeSelector);
        }

        this.view = null;

        this.themeModel?.unsubscribe("themeChanged", this.boundOnThemeChanged);
        this.themeModel = null;
    }

    private handleThemeSelectorTriggered(e: Event): void {
        if (e instanceof CustomEvent && this.themeModel) {
            const action = e.detail.action;
            if (action) {
                this.themeModel.theme = action;
            }
        }
    }

    private onThemeChanged(e: Event): void {
        if (e instanceof CustomEvent) {
            const theme = e.detail.theme;
            if (theme && this.view) {
                for (const item of this.view.querySelectorAll(".theme-selector")) {
                    item.setAttribute("selectedAction", theme);
                }
            }
        }
    }

    private setVersion(view: ShadowRoot, id: string, version?: string): void {
        const elm = view.getElementById(id);
        if (elm && version) {
            elm.innerText = version;
        }
    }
}
