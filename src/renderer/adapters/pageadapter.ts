// SPDX-FileCopyrightText: 2021 Andrew Hayzen <ahayzen@gmail.com>
//
// SPDX-License-Identifier: MPL-2.0

import { MappedInterface } from "../helpers";
import { IAdapter } from "./index";
import { IModel } from "../wasm";

// The type of history change that should occur when loading the new page
export const enum PageHistoryState {
    PushState,
    ReplaceState,
    NoState,
}

// TODO: handle subpages etc ? this assume everything after #/ is the page name
const hashRegex = /^#\/(?<page>.*)$/;

export class PageAdapter implements IAdapter {
    private currentPage: string | null = null;
    private menuElement: HTMLElement | null = null;
    private routerElement: HTMLElement | null = null;
    private view: Window | null = null;

    private boundHandleHashChange: () => void;
    private boundHandleMenuClick: (e: Event) => void;
    private boundHandlePopState: (ev: PopStateEvent) => void;

    constructor() {
        this.boundHandleHashChange = this.handleHashChange.bind(this);
        this.boundHandleMenuClick = this.handleMenuClick.bind(this);
        this.boundHandlePopState = this.handlePopState.bind(this);
    }

    async attach(view: Window, _models: MappedInterface<IModel>): Promise<void> {
        this.view = view;

        // Load the menu and router elements
        // these remain static between page changes so can be loaded here
        this.menuElement = view.document.getElementById("menu");
        this.routerElement = view.document.getElementById("router");

        // When the menu is clicked, load the relevant page
        this.menuElement?.addEventListener("triggered", this.boundHandleMenuClick);

        // Listen for back being activated in the browser
        view.addEventListener("popstate", this.boundHandlePopState);

        // Listen for the hash part of the url changing to change to that page
        view.addEventListener("hashchange", this.boundHandleHashChange);
    }

    async detach(view: Window, _models: MappedInterface<IModel>): Promise<void> {
        view.removeEventListener("hashchange", this.boundHandleHashChange);
        view.removeEventListener("popstate", this.boundHandlePopState);

        this.menuElement?.removeEventListener("triggered", this.boundHandleMenuClick);

        this.routerElement = null;
        this.menuElement = null;
        this.view = null;
    }

    private handleHashChange(): void {
        const page = this.hashToPage();
        if (page) {
            this.loadPage(page, PageHistoryState.PushState);
        }
    }

    // When the menu is clicked, load the page
    private handleMenuClick(e: Event): void {
        const action: string | undefined = (e as CustomEvent).detail.action;
        if (action) {
            this.loadPage(action, PageHistoryState.PushState);
        }
    }

    // When back is pressed in the browser, intercept and load the page
    private handlePopState(ev: PopStateEvent): void {
        if (ev.state) {
            const page: string | undefined = ev.state.page;
            if (page) {
                ev.preventDefault();

                this.loadPage(page, PageHistoryState.NoState);
            }
        }
    }

    hashToPage(): string | undefined {
        return this.view?.location.hash.match(hashRegex)?.groups?.page;
    }

    loadPage(page: string, historyState: PageHistoryState = PageHistoryState.PushState): void {
        if (page !== this.currentPage) {
            const href = this.pageToHref(page);
            this.currentPage = page;

            // Update the selected action in the menu
            this.menuElement?.setAttribute("selectedAction", page);
            // Update the page of the router
            this.routerElement?.setAttribute("route", page);

            // Push or replace the page change to the browser history
            switch (historyState) {
                case PageHistoryState.NoState:
                    break;
                case PageHistoryState.PushState: {
                    this.view?.history.pushState(
                        {
                            href: href,
                            page: page,
                        },
                        page,
                        href
                    );
                    break;
                }
                case PageHistoryState.ReplaceState: {
                    this.view?.history.replaceState(
                        {
                            href: href,
                            page: page,
                        },
                        page,
                        href
                    );
                    break;
                }
                default: {
                    console.warn("Unknown PageHistoryState: ", historyState);
                    break;
                }
            }
        }
    }

    // Convert a page name to a href
    private pageToHref(page: string): string {
        return "#/" + page;
    }
}
