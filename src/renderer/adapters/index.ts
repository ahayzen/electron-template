// SPDX-FileCopyrightText: 2021 Andrew Hayzen <ahayzen@gmail.com>
//
// SPDX-License-Identifier: MPL-2.0

import { MappedInterface } from "../helpers";
import { IModel } from "../wasm";

export interface IAdapter {
    attach(view: Window, models: MappedInterface<IModel>): Promise<void>;
    detach(view: Window, models: MappedInterface<IModel>): Promise<void>;
}

export * from "./electronadapter";
export * from "./pageadapter";
export * from "./themeadapter";
