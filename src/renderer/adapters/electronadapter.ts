// SPDX-FileCopyrightText: 2021 Andrew Hayzen <ahayzen@gmail.com>
//
// SPDX-License-Identifier: MPL-2.0

import { MappedInterface } from "../helpers";
import { IAdapter } from "./index";
import { IModel, ThemeModel } from "../wasm";

export class ElectronAdapter implements IAdapter {
    private boundOnThemeChanged: (e: Event) => void;
    private view: Window | null = null;

    constructor() {
        this.boundOnThemeChanged = this.onThemeChanged.bind(this);
    }

    async attach(view: Window, models: MappedInterface<IModel>): Promise<void> {
        this.view = view;

        models.getType(ThemeModel, "theme")?.subscribe("themeChanged", this.boundOnThemeChanged);
    }

    async detach(_view: Window, models: MappedInterface<IModel>): Promise<void> {
        models.getType(ThemeModel, "theme")?.unsubscribe("themeChanged", this.boundOnThemeChanged);

        this.view = null;
    }

    private onThemeChanged(e: Event): void {
        // Extract new theme and inform electron
        //
        // This affects context menus, devtols, window frames, CSS prefer-color-scheme
        if (e instanceof CustomEvent) {
            const theme = e.detail.theme;
            if (theme === "system" || theme === "dark" || theme === "light") {
                this.view?.electron?.nativeTheme.themeSource(theme);
            }
        }
    }
}
