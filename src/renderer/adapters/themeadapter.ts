// SPDX-FileCopyrightText: 2021 Andrew Hayzen <ahayzen@gmail.com>
//
// SPDX-License-Identifier: MPL-2.0

import { MappedInterface } from "../helpers";
import { IAdapter } from "./index";
import { IModel, ThemeModel } from "../wasm";

export class ThemeAdapter implements IAdapter {
    private boundOnThemeChanged: (e: Event) => void;
    private view: Window | null = null;

    constructor() {
        this.boundOnThemeChanged = this.onThemeChanged.bind(this);
    }

    async attach(view: Window, models: MappedInterface<IModel>): Promise<void> {
        this.view = view;

        const themeModel = models.getType(ThemeModel, "theme");
        if (themeModel) {
            // Listen for any changes
            themeModel.subscribe("themeChanged", this.boundOnThemeChanged);

            // Load initial theme
            this.updateCSS(themeModel.theme);
        }
    }

    async detach(_view: Window, models: MappedInterface<IModel>): Promise<void> {
        models.getType(ThemeModel, "theme")?.unsubscribe("themeChanged", this.boundOnThemeChanged);

        this.view = null;
    }

    // When the theme is changed, tell CSS to update
    private onThemeChanged(e: Event): void {
        if (e instanceof CustomEvent) {
            const theme = e.detail.theme;
            if (theme) {
                this.updateCSS(theme);
            }
        }
    }

    // Update the global CSS themes
    private updateCSS(theme: string): void {
        if (this.view) {
            // If the theme is dark or light then set to the body
            if (theme === "dark" || theme === "light") {
                this.view.document.body.setAttribute("data-user-theme", theme);
            } else {
                // Otherwise remove the attribute as we are system
                this.view.document.body.removeAttribute("data-user-theme");
            }
        }
    }
}
