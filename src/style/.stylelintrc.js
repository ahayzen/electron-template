// SPDX-FileCopyrightText: 2021 Andrew Hayzen <ahayzen@gmail.com>
//
// SPDX-License-Identifier: CC0-1.0

module.exports = {
    extends: ["stylelint-config-standard", "stylelint-config-prettier"],
    plugins: ["stylelint-no-unsupported-browser-features", "stylelint-order", "stylelint-scss"],
    rules: {
        // Disable CSS at rule checker as use @ in scss, we enable the scss one below
        "at-rule-no-unknown": null,
        // Order css code
        "order/order": ["custom-properties", "dollar-variables", "declarations", "rules", "at-rules"],
        "order/properties-alphabetical-order": true,
        // Check we are compatible with our browserlist
        "plugin/no-unsupported-browser-features": [
            true,
            {
                ignore: [],
                ignorePartialSupport: true,
            },
        ],
        // Enable the scss at rule checker
        "scss/at-rule-no-unknown": true,
        // Ignore custom-* css types from our components
        "selector-type-no-unknown": [
            true,
            {
                ignoreTypes: ["/^custom-*/"],
            },
        ],
    },
};
