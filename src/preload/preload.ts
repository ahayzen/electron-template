// SPDX-FileCopyrightText: 2021 Andrew Hayzen <ahayzen@gmail.com>
//
// SPDX-License-Identifier: MPL-2.0

import { contextBridge, ipcRenderer } from "electron";

contextBridge.exposeInMainWorld("electron", {
    nativeTheme: {
        themeSource: async (theme: "system" | "dark" | "light") => {
            await ipcRenderer.invoke("nativeTheme::themeSource", theme);
        },
    },
    versions: {
        chrome: process.versions.chrome,
        electron: process.versions.electron,
        node: process.versions.node,
    },
});
