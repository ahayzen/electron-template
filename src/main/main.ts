// SPDX-FileCopyrightText: 2021 Andrew Hayzen <ahayzen@gmail.com>
//
// SPDX-License-Identifier: MPL-2.0

import { app, BrowserWindow, ipcMain, IpcMainInvokeEvent, Menu, nativeTheme } from "electron";
import path = require("path");

function createWindow(): void {
    const production: boolean = process.env.NODE_ENV === "production";

    const win = new BrowserWindow({
        autoHideMenuBar: true,
        backgroundColor: "#111111",
        height: 600,
        frame: true,
        minHeight: 300,
        minWidth: 400,
        show: false,
        webPreferences: {
            // Disable devTools in production
            devTools: !production,
            contextIsolation: true,
            nodeIntegration: false,
            preload: path.join(app.getAppPath(), "preload.js"),
            sandbox: true,
        },
        width: 800,
    });

    // Disable menubar in production
    if (production) {
        Menu.setApplicationMenu(null);
    }

    // Show the Window only once it's ready
    // this prevents a flicker as the initial page is loading
    win.once("ready-to-show", () => {
        win.show();
    });

    // List for theme requests
    ipcMain.handle("nativeTheme::themeSource", (_event: IpcMainInvokeEvent, theme: "system" | "dark" | "light") => {
        nativeTheme.themeSource = theme;
    });

    // Set the page to load
    win.loadFile("index.html");
}

app.whenReady().then(createWindow);

app.on("activate", () => {
    if (BrowserWindow.getAllWindows().length === 0) {
        createWindow();
    }
});
app.on("window-all-closed", () => {
    if (process.platform !== "darwin") {
        app.quit();
    }
});
