#!/usr/bin/env bash

# SPDX-FileCopyrightText: 2021 Andrew Hayzen <ahayzen@gmail.com>
#
# SPDX-License-Identifier: MPL-2.0

set -e

mkdir -p build/bundle/
mkdir -p build/compile/
mkdir -p build/minify/

#
# Build JavaScript and wasm
#

echo "Build components..."
tsc --build --incremental src/components/tsconfig.json
rollup --config src/components/rollup.config.js
terser --source-map "content='build/bundle/components/components.js.map',url='components.js.map'" --output build/minify/components.js build/bundle/components/components.js

echo "Build renderer:wasm ..."
cargo build --manifest-path ./src/wasm/Cargo.toml --release --target wasm32-unknown-unknown
wasm-bindgen --out-dir ./build/compile/renderer --out-name wasm --target web --weak-refs ./src/wasm/target/wasm32-unknown-unknown/release/hello_wasm.wasm
cp build/compile/renderer/wasm_bg.wasm build/minify/

echo "Build renderer:typescript ..."
tsc --build --incremental src/renderer/tsconfig.json
rollup --config src/renderer/rollup.config.js
terser --source-map "content='build/bundle/renderer/renderer.js.map',url='renderer.js.map'" --output build/minify/renderer.js build/bundle/renderer/renderer.js

#
# Build HTML, CSS, and static files
#

echo "Build html ..."
html-minifier-terser --config-file src/html/terser.config.json --file-ext html --input-dir src/html --output-dir build/minify/

echo "Build style ..."
sass --embed-sources --source-map \
    src/style/common.scss:build/compile/style/common.css \
    src/style/root.scss:build/compile/style/root.css \
    src/style/pages/wasm.scss:build/compile/style/pages/wasm.css
cleancss --input-source-map build/compile/style/common.css.map --source-map --source-map-inline-sources --output build/minify/common.css --with-rebase build/compile/style/common.css
cleancss --input-source-map build/compile/style/root.css.map --source-map --source-map-inline-sources --output build/minify/root.css --with-rebase build/compile/style/root.css
cleancss --input-source-map build/compile/style/pages/wasm.css.map --source-map --source-map-inline-sources --output build/minify/pages/wasm.css --with-rebase build/compile/style/pages/wasm.css

echo "Build static ..."
imagemin src/static/**/*.svg --out-dir=build/minify/static

#
# Build electron parts
#

echo "Build main ..."
tsc --build --incremental src/main/tsconfig.json
terser --source-map "content='build/compile/main/main.js.map',url='main.js.map'" --output build/minify/main.js build/compile/main/main.js

echo "Build preload ..."
tsc --build --incremental src/preload/tsconfig.json
terser --source-map "content='build/compile/preload/preload.js.map',url='preload.js.map'" --output build/minify/preload.js build/compile/preload/preload.js

echo "Copy electron-builder metadata"
cp src/package.json build/minify/package.json

#
# Build Tests
#

echo "Build acceptance tests ..."
# We only build as commonjs non-bundled for now otherwise mocha has issues running
# as it is confused by the dynamic imports, even with require=esm.
#
# As this is for tests it does not matter that they are not bundled and minified
tsc --build --incremental tests/tsconfig.json

echo "Build components unit tests ..."
tsc --build --incremental src/components/tests/tsconfig.json
rollup --config src/components/tests/rollup.config.js
terser --source-map "content='build/bundle/components/tests/components_test.js.map',url='unit_components.js.map'" --output build/minify/unit_components.js build/bundle/components/tests/components_test.js

echo "Build renderer unit tests ..."
tsc --build --incremental src/renderer/tests/tsconfig.json
rollup --config src/renderer/tests/rollup.config.js
terser --source-map "content='build/bundle/renderer/tests/renderer_test.js.map',url='unit_renderer.js.map'" --output build/minify/unit_renderer.js build/bundle/renderer/tests/renderer_test.js
